package main

import (
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	var total int = 0
	var totalsArr []int

	content, err := os.ReadFile("in.txt")
	if err != nil {
		log.Fatal(err)
	}
	newStrings := strings.Split(string(content), "\n")
	for _, val := range newStrings {
		if len(val) == 0 {
			fmt.Printf("Adding total %d to totalsArr \n", total)
			totalsArr = append(totalsArr, total)
			total = 0

			continue
		}

		intVal, err := strconv.Atoi(val)
		if err != nil {
			panic(err)
		}

		total = total + intVal
	}

	sort.Ints(totalsArr)
	fmt.Println(totalsArr)
	var threeLargestTotal = totalsArr[len(totalsArr)-1] + totalsArr[len(totalsArr)-2] + totalsArr[len(totalsArr)-3]
	var largest = totalsArr[len(totalsArr)-1];

	fmt.Printf("Largest elf: %d", largest)
	fmt.Printf("Total for three best elves: %d", threeLargestTotal)
}
