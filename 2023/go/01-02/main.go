package main

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

func loadInput() []string {
	content, err := os.ReadFile("data.txt")
	if err != nil {
		log.Fatal(err)
	}
	newStrings := strings.Split(string(content), "\n")
	return newStrings
}

func getAllIndexesOfSubstring(line string, substring string) []int {
	fmt.Printf("Attempting to find %s in %s\n", substring, line)
	var instances []int

	r := regexp.MustCompile(fmt.Sprintf("%s", substring))
	matches := r.FindAllStringIndex(line, -1)
	for _, v := range matches {
		instances = append(instances, v[0])
	}

	return instances
}

func cleanLine(line string) string {
	numStrings := map[string]string{
		"zero":  "0",
		"one":   "1",
		"two":   "2",
		"three": "3",
		"four":  "4",
		"five":  "5",
		"six":   "6",
		"seven": "7",
		"eight": "8",
		"nine":  "9",
	}
	indexes := map[int]string{}

	println(line)
	for index, char := range numStrings {
		instances := getAllIndexesOfSubstring(line, char)
		instances = append(instances[:], getAllIndexesOfSubstring(line, index)...)

		for _, ind := range instances {
			indexes[ind] = char
		}
	}

	keys := make([]int, 0, len(indexes))

	for k := range indexes {
		keys = append(keys, k)
	}
	sort.Sort(sort.IntSlice(keys))
	println("-----------------------")
	for _, value := range keys {
		fmt.Printf("%v : %s\n", value, indexes[value])
	}

	return fmt.Sprintf("%s%s", indexes[keys[0]], indexes[keys[len(keys)-1]])
}

func main() {
	input := loadInput()
	println(input)
	total := 0
	for _, rawLine := range input {
		if len(rawLine) > 0 {
			line := cleanLine(rawLine)
			fmt.Printf("%s\n", line)

			strTotal, _ := strconv.Atoi(line)
			total += strTotal

		}
	}
	println(total)
}
