package main

import (
	"log"
	"os"
	"strconv"
	"strings"
)

func loadInput() []string {
	content, err := os.ReadFile("data.txt")
	if err != nil {
		log.Fatal(err)
	}
	newStrings := strings.Split(string(content), "\n")
	return newStrings
}

func main() {
	input := loadInput()
	println("Hello, World!")
	println(input)
	total := 0
	for lineNum, line := range input {
		num := ""
		for _, char := range line {
			if _, err := strconv.Atoi(string(char)); err == nil {
				num += string(char)
			}
		}
		if len(num) == 0 {
			continue
		}
		println("-------------", lineNum, "-----------------")
		println(line)
		formString := num[0:1] + num[len(num)-1:]
		strTotal, _ := strconv.Atoi(formString)
		println("num: ", strTotal, "total: ", total)
		total += strTotal
	}
	println(total)
}

