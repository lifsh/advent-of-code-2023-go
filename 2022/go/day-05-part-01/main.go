package main

import (
	"fmt"
	"log"
	"os"
	"strings"
)

func loadInput() []string {
	content, err := os.ReadFile("in.txt")
	if err != nil {
		log.Fatal(err)
	}
	newStrings := strings.Split(string(content), "\n")
	return newStrings
}

func main() {
	log.Printf("Test")
	input := loadInput()
	var crates [][]string
	var cratesInput []string
	var instructionsInput []string
	var cratesStored = false
	for _, val := range input {
		if len(val) == 0 {
			cratesStored = true
		}

		if cratesStored == false {
			cratesInput = append(cratesInput, val)
		}
		if cratesStored == true {
			instructionsInput = append(instructionsInput, val)
		}

		log.Printf(val)
	}

	for i := len(cratesInput) - 1; i >= 0; i = i - 1 {
		// log.Printf(cratesInput[i])
		var currentEntry = ""
		var entryIndex = 0
		var tempCrateRow = []string{}
		log.Printf("=========================")
		for index, char := range cratesInput[i] {
			currentEntry = currentEntry + string(char)
			// log.Printf(strconv.Itoa(index+1) + " - " + strconv.Itoa((index+1)%4))
			if (index+1)%4 == 0 {
				log.Printf(currentEntry)
				tempCrateRow = append(tempCrateRow, currentEntry)
				currentEntry = ""
			}
			entryIndex = entryIndex + 1
			// log.Printf(string(char))
		}
		crates = append(crates, tempCrateRow)
	}
	for _, row := range crates {
		fmt.Println(row)
	}
}
