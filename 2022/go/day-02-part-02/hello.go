package main

import (
	"fmt"
	"log"
	"os"
	"strings"
)

const DRAW_SCORE = 3
const WIN_SCORE = 6
const LOSS_SCORE = 0

func loadInput() []string {
	content, err := os.ReadFile("in.txt")
	if err != nil {
		log.Fatal(err)
	}
	newStrings := strings.Split(string(content), "\n")
	return newStrings
}

// Rock A
// Paper B
// Scissor C

// Rock X
// Paper Y
// Scissor Z
func calcScore(opponentMove, desiredOutcome string) int {
	winCondition := map[string]string{
		"A": "B",
		"B": "C",
		"C": "A",
	}
	playerActions := map[string]string{
		"X": winCondition[winCondition[opponentMove]],
		"Y": opponentMove,
		"Z": winCondition[opponentMove],
	}
	actionScore := map[string]int{
		"A": 1,
		"B": 2,
		"C": 3,
	}
	var playerMove = playerActions[desiredOutcome]

	if winCondition[opponentMove] == playerMove {
		fmt.Printf("Player WIN  with %s against %s \n", opponentMove, playerMove)
		return WIN_SCORE + actionScore[playerMove]
	}

	if opponentMove == playerMove {
		fmt.Printf("Player DRAW with %s against %s \n", opponentMove, playerMove)
		return DRAW_SCORE + actionScore[playerMove]
	}

	fmt.Printf("Player LOSE with %s against %s \n", opponentMove, playerMove)
	return LOSS_SCORE + actionScore[playerMove]
}

func main() {
	matches := loadInput()
	var total = 0
	for _, val := range matches {
		moves := strings.Split(val, " ")
		if len(moves) != 2 {
			continue
		}
		var score = calcScore(moves[0], moves[1])
		total = total + score
		fmt.Printf("Score %d \n", score)
	}
	fmt.Printf("Total score: %d \n", total)
}
