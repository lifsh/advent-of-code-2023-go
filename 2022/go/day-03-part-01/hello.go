package main

import (
	"fmt"
	"log"
	"os"
	"strings"
)

const SCORE_MAP = "abcdefghijklmnopqrstuvwxyz"

func loadInput() []string {
	content, err := os.ReadFile("in.txt")
	if err != nil {
		log.Fatal(err)
	}
	newStrings := strings.Split(string(content), "\n")
	return newStrings
}

func getOverlap(first, second string) string {
	fmt.Printf("Checking %s : %s \n", first, second)
	for _, val := range strings.Split(second, "") {
		if strings.Contains(first, val) {
			return val
		}
	}

	panic("No overlap found")
}

func calcScore(char string) int {
	if strings.Index(SCORE_MAP, char) > -1 {
		return strings.Index(SCORE_MAP, char) + 1
	}

	return strings.Index(strings.ToUpper(SCORE_MAP), char) + len(SCORE_MAP) + 1
}

func main() {
	var total = 0
	matches := loadInput()
	println(matches)
	for _, val := range matches {
		if len(val) == 0 {
			continue
		}
		firstHalf := val[0 : len(val)/2]
		secondHalf := val[len(val)/2:]
		var char = getOverlap(firstHalf, secondHalf)
		total = total + calcScore(char)
		fmt.Println(firstHalf)
	}
	fmt.Printf("Total score: %d \n", total)
}
